cls
@ECHO OFF

REM CONFIGURATION - MACROQUEST DIR
set EQDIR=C:\Games\EqProfusion\Everquest_ROF2
REM CONFIGURATION - MACROQUEST DIR
SET MQ2DIR=C:\Games\EqProfusion\MQ2Emu_ROF2

REM CHAR CONF - SET EACH AND DUPLICATE THIS AS MUCH AS NECESSARY
set Arr[0][0]=account1
set Arr[0][1]=toon1
set Arr[0][2]="C:\Games\EqProfusion\Launcher\EQ - ProF\EQ Rof2 - 03Guizmo.lnk"

set Arr[1][0]=account2
set Arr[1][1]=toon2
set Arr[1][2]="C:\Games\EqProfusion\Launcher\EQ - ProF\EQ Rof2 - 04Sherly.lnk"

set Arr[2][0]=account3
set Arr[2][1]=toon3
set Arr[2][2]="C:\Games\EqProfusion\Launcher\EQ - ProF\EQ Rof2 - 05Lugorthin.lnk"

set Arr[3][0]=account4
set Arr[3][1]=toon4
set Arr[3][2]="C:\Games\EqProfusion\Launcher\EQ - ProF\EQ Rof2 - 06Mogwai.lnk"


REM ENABLE COMMAND EXTENSION
SETLOCAL EnableExtensions

REM AUTO START EQBCS IF NEEDED
IF NOT EXIST "%MQ2DIR%\EQBCServer.exe" goto EQBCServerNOTFOUND
set EXE=EQBCServer.exe
FOR /F %%x IN ('tasklist /NH /FI "IMAGENAME eq %EXE%"') DO IF %%x == %EXE% goto EQBCServerFOUND
ECHO LAUNCH EQBCS
start "EQBCServer" /high /D"%MQ2DIR%" "%MQ2DIR%\EQBCServer.exe"
:EQBCServerFOUND

REM AUTO START MACROQUEST2 IF NEEDED
IF NOT EXIST "%MQ2DIR%\MacroQuest2.exe" goto MacroQuest2FOUND
set EXE=MacroQuest2.exe
FOR /F %%x IN ('tasklist /NH /FI "IMAGENAME eq %EXE%"') DO IF %%x == %EXE% goto MacroQuest2FOUND
ECHO LAUNCH MACROQUEST2
start "MacroQuest2" /D"%MQ2DIR%" "%MQ2DIR%\%EXE%"
:MacroQuest2FOUND

set "x=0"

:SymLoop
    if not defined Arr[%x%][0] goto :endLoop
    call set ACCOUNT=%%Arr[%x%][0]%%
    call set TOON=%%Arr[%x%][1]%%
    call set SHORTCUT=%%Arr[%x%][2]%%
    REM do your stuff VAL
    
    @ECHO Launching %TOON%
    if not defined Arr[%x%][2] goto :runnormal
        @ECHO Launching %SHORTCUT%
        start "" /b cmd /c "%SHORTCUT%"
        timeout /t 2
        SET /a "x+=1"
        GOTO :SymLoop
    :runnormal
        start "EQ %TOON%" /D%EQDIR% "%EQDIR%\eqgame.exe" patchme /login:%ACCOUNT% /name:%TOON%
        timeout /t 2
        SET /a "x+=1"
        GOTO :SymLoop

:endLoop
echo "Done"